package main;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.InHouse;
import model.Inventory;
import model.Outsourced;
import model.Product;

/**
 * This class is the entry point of the Inventory Management application.
 *
 * @author Devin Craig
 */
public class Main extends Application {
    Stage window;

    /**
     * Initializes the window and starts the program.
     * @param primaryStage primaryStage provided by initializing the javafx application.
     * @throws Exception Throws an Exception if main view can not be loaded.
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        initialize();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/styles/defaults.css").toExternalForm());
        window = primaryStage;
        window.setTitle("Inventory Management System - Devin Craig");
        window.setScene(scene);
        window.show();
    }

    /**
     * Generates sample data for use in testing the program.
     */
    private void initialize() {
        InHouse IHpart1 = new InHouse(1, "Laptop Charger", 59.99, 35, 10, 40, 74);
        InHouse IHpart2 = new InHouse(2, "Laptop Charger 2", 60, 3, 1, 42, 75);
        Outsourced osPart1 = new Outsourced(3, "Laptop Screen", 152.84, 339, 1, 1000, "Billy's Laptop Screen Company");
        Outsourced osPart2 = new Outsourced(4, "Phone Case", 15.00, 100, 1, 300, "Awesome Phone Accessories, Inc.");
        Product prod1 = new Product(FXCollections.observableArrayList(), 4, "Awesome Product 1", 1234.00, 5, 3, 200);
        Product prod2 = new Product(FXCollections.observableArrayList(), 1, "Awesome Product 2", 14.00, 250, 3, 310);
        prod1.getAssociatedParts().addAll(IHpart1, osPart2);
        prod2.getAssociatedParts().addAll(IHpart2, osPart1);

        Inventory.getAllParts().addAll(IHpart1, IHpart2, osPart1, osPart2);
        Inventory.getAllProducts().addAll(prod1, prod2);
    }

    /**
     * Application entry point.
     * @param args Argument strings passed to the application.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
