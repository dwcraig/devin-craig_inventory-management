package main;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This class is the confirmation dialog that comes up when cancelling changes or deleting inventory.
 *
 * @author Devin Craig
 */
public class ConfirmationBox {
    static Boolean response = false;

    /**
     * Provides a convenient way to ask the user to confirm an action.
     *
     * @param title The title of the confirmation dialog Stage.
     * @param message The message you would like to display to the user.
     * @return true if the user confirms the action.
     */
    public static Boolean display(String title, String message) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);
        window.setMinHeight(150);
        Label label = new Label();
        label.setText(message);
        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        yesButton.setOnAction(e -> {
            response = true;
            window.close();
        });

        noButton.setOnAction(e -> {
            response = false;
            window.close();
        });

        window.setOnCloseRequest(e -> {
            e.consume();
            response = false;
            window.close();
        });


        VBox layout = new VBox(10);
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(label, yesButton, noButton);
        Scene scene = new Scene(layout);
        scene.getStylesheets().add(ConfirmationBox.class.getResource("/styles/defaults.css").toExternalForm());
        window.setScene(scene);
        window.showAndWait();

        return response;
    }
}
