package controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.*;
import java.io.IOException;
import java.text.NumberFormat;

/**
 * The Modify Product Controller handles the functionality of the modify product view.  This class loads the current product's information into the fields and tableviews, and validates and saves parts into inventory.
 *
 * @author Devin Craig
 */
public class ModifyProductController {

    Stage window;
    Parent scene;

    /**
     * Saves a reference to the product passed in order to keep it in scope.
     */
    Product tempProduct;
    /**
     * Stores parts that are filtered by for the search functionality.
     */
    ObservableList<Part> partSearchList = FXCollections.observableArrayList();
    /**
     * Stores temporary changes before writing them back to the product permanently.
     */
    ObservableList<Part> tempAssociatedParts = FXCollections.observableArrayList();

    @FXML
    private Button cancelButton;

    @FXML
    private TextField productID;

    @FXML
    private TextField productName;

    @FXML
    private TextField productInventory;

    @FXML
    private TextField productPrice;

    @FXML
    private TextField productMinInventory;

    @FXML
    private TextField productMaxInventory;

    @FXML
    private TextField productSearch;

    @FXML
    private TableView<Part> partsTable;

    @FXML
    private TableColumn<Part, Integer> partIdTableColumn;

    @FXML
    private TableColumn<Part, String> partNameTableColumn;

    @FXML
    private TableColumn<Part, Integer> partInventoryTableColumn;

    @FXML
    private TableColumn<Part, Double> partPriceTableColumn;

    @FXML
    private TableView<Part> partsInProductTable;

    @FXML
    private TableColumn<Part, Integer> partIDProductTableColumn;

    @FXML
    private TableColumn<Part, String> partNameProductTableColumn;

    @FXML
    private TableColumn<Part, Integer> partInventoryProductTableColumn;

    @FXML
    private TableColumn<Part, Double> partPriceProductTableColumn;

    @FXML
    private Button addPartButton;

    @FXML
    private Button saveButton;

    @FXML
    private Button removePartButton;

    @FXML
    private Label errorLabel;

    /**
     * Adds a part to the temporary list, to be saved to the part once the user clicks the save button.
     * Only add the part if it does not already exist in the product.
     *
     * @param event Add Part button click event.
     */
    @FXML
    void addPart(MouseEvent event) {
        errorLabel.setText("");
        Part newPart = partsTable.getSelectionModel().getSelectedItem();
        if(newPart == null) {
            errorLabel.setText("Please select a part.");
            return;
        }
        for(Part part : tempAssociatedParts) {
            if(newPart.getId() == part.getId()) {
                return;
            }
        }
        tempAssociatedParts.add(newPart);
    }

    /**
     * Cancels the changes the user has made without making changes to the product.
     * Requires the user to confirm cancelling the changes.
     *
     * @param event Cancel button click event.
     * @throws IOException Throws an IOException if the main view can not be loaded.
     */
    @FXML
    void cancelButton(MouseEvent event) throws IOException {
        if(main.ConfirmationBox.display("Cancel", "Are you sure you want to cancel?")) {
            window = (Stage) ((Button) event.getSource()).getScene().getWindow();
            scene = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
            window.setScene(new Scene(scene));
            window.show();
        }
    }

    /**
     * Removes a part from the temporary list of associated parts.  Saved to the part when the user clicks the save button.
     * @param event Remove part click event.
     */
    @FXML
    void removePart(MouseEvent event) {
        errorLabel.setText("");
        Part part = partsInProductTable.getSelectionModel().getSelectedItem();
        if(part == null) {
            errorLabel.setText("Please select a part to remove.");
        }
        tempAssociatedParts.remove(part);
    }

    /**
     * Saves the product in the Inventory, after checking the field values are within bounds.
     * Returns to the main window.
     *
     * @param event Save product button click event.
     * @throws IOException Throws an IOException if the main view can not be loaded.
     */
    @FXML
    void saveProduct(MouseEvent event) throws IOException {
        try {
            checkValues();
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
            return;
        }

        tempProduct.setName(productName.getText());
        tempProduct.setPrice(Double.parseDouble(productPrice.getText()));
        tempProduct.setStock(Integer.parseInt(productInventory.getText()));
        tempProduct.setMin(Integer.parseInt(productMinInventory.getText()));
        tempProduct.setMax(Integer.parseInt(productMaxInventory.getText()));
        tempProduct.getAssociatedParts().clear();
        tempAssociatedParts.forEach(part -> tempProduct.getAssociatedParts().add(part));

        window = (Stage)((Button)event.getSource()).getScene().getWindow();
        scene = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
        scene.getStylesheets().add(getClass().getResource("/styles/defaults.css").toExternalForm());
        window.setScene(new Scene(scene));
        window.show();
    }

    /**
     * Helper to run validation checks on field data.
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkValues() throws Exception{
        checkName();
        checkInventory();
        checkPrice();
        checkParts();
    }

    /**
     * Checks that there are 1 or more parts in the associated parts in order to save the product.
     *
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkParts() throws Exception{
        if(tempAssociatedParts.isEmpty()) {throw new Exception("Products must have at least 1 part.");}
    }

    /**
     * Checks the following:
     * Price field is not empty
     * Properly parsed as a Double
     * Price is a positive Double
     * Price of the product is greater than the sum of the price of the parts
     *
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkPrice() throws Exception {
        Double price;
        try {
            price = Double.parseDouble(productPrice.getText());
        } catch (NumberFormatException e) {
            throw new Exception("Price must be a double");
        }

        if(price < 0) {throw new Exception("Price must be a positive value");}


        Double totalCost = 0.0;
        for(Part part : tempAssociatedParts) {
            totalCost += part.getPrice();
        }

        if(totalCost > price) {
            throw new Exception("Price must be great than cost of parts");
        }
    }

    /**
     * Completes the following checks on Inventory fields (stock, max, min):
     * Not empty
     * Min is less than Stock and Stock is less than Max
     * All fields can properly be parsed to Integers.
     *
     * @throws Exception to display in the error label to indicate what went wrong for the user.
     */
    private void checkInventory() throws Exception{
        if(productInventory.getText().isEmpty()) {throw new Exception("Inventory can't be empty.");}
        if(productMinInventory.getText().isEmpty()) {throw new Exception("Minimum Inventory can't be empty.");}
        if(productMaxInventory.getText().isEmpty()) {throw new Exception("Maximum Inventory can't be empty.");}

        try {
            int inv = Integer.parseInt(productInventory.getText());
            int minInv = Integer.parseInt(productMinInventory.getText());
            int maxInv = Integer.parseInt(productMaxInventory.getText());

            if(minInv > inv || maxInv < inv) {throw new Exception("Inventory must be between min and max");}
            if(minInv < 0 || inv < 0) {throw new Exception("Inventory must be a positive integer.");}

        } catch (NumberFormatException e){
            throw new Exception("All inventory values must be integers.");
        }
    }

    /**
     * Checks that the product name field is not an empty string.
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkName() throws Exception{
        if(productName.getText().isEmpty()) {throw new Exception("Name can't be empty.");}
    }

    /**
     * Filters the list of parts based on Name or Id
     * @param event Search parts key event, triggered each time a letter is changed in the search field.
     */
    @FXML
    void searchParts(KeyEvent event) {
        partSearchList.clear();
        for(Part part : Inventory.getAllParts()) {
            Integer id;
            try {
                id = Integer.parseInt(productSearch.getText());
            } catch (NumberFormatException e) {
                id = -1;
            }
            if(part.getId() == id || part.getName().toLowerCase().contains(productSearch.getText().toLowerCase())) {
                partSearchList.add(part);
            }
        }

        partsTable.setItems(partSearchList);
    }

    /**
     * Used to pass a selected product to the modify product controller.  Initializes TextFields and TableViews.
     * @param product Receives a product to modify from the caller.
     */
    public void sendProduct(Product product) {
        tempProduct = product;

        /**
         * After modifying a part which has been added to a product, I spend a significant amount of time working through
         * how to update the parts associated with a product.  Initially, I expected the ObservableList to handle this
         * through listeners to parts changing.  This may still have been a better way, but I found that it was possible
         * to simply update the values of the parts in the associated parts list based on the actual inventory list at the
         * time of loading the modify product view.  This allows the views to be accurately reflected with up to date changes.
         */
        tempProduct.getAssociatedParts().forEach(p -> {
            for(Part part : Inventory.getAllParts()) {
                if (p.getId() == part.getId()) {
                    tempAssociatedParts.add(part);
                }
            }
        });

        productID.setText(String.valueOf(product.getId()));
        productName.setText(product.getName());
        productInventory.setText(String.valueOf(product.getStock()));
        productPrice.setText(String.valueOf(product.getPrice()));
        productMinInventory.setText(String.valueOf(product.getMin()));
        productMaxInventory.setText(String.valueOf(product.getMax()));

        partsTable.setItems(Inventory.getAllParts());
        partIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInventoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceTableColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        partsTable.getSortOrder().add(partIdTableColumn);
        //Format Price https://stackoverflow.com/questions/48733121/javafx-format-double-in-tablecolumn
        NumberFormat partCurrencyFormat = NumberFormat.getCurrencyInstance();
        partPriceTableColumn.setCellFactory(tc -> new TableCell<Part, Double>(){
            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(partCurrencyFormat.format(price));
                }
            }
        });

        partsInProductTable.setItems(tempAssociatedParts);
        partIDProductTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameProductTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInventoryProductTableColumn.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceProductTableColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        partsInProductTable.getSortOrder().add(partIDProductTableColumn);
        //Format Price https://stackoverflow.com/questions/48733121/javafx-format-double-in-tablecolumn
        partPriceTableColumn.setCellFactory(tc -> new TableCell<Part, Double>(){
            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(partCurrencyFormat.format(price));
                }
            }
        });
    }
}



