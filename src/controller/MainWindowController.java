package controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Inventory;
import model.Part;
import model.Product;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ResourceBundle;

/**
 * The Main Window Controller handles the functionality for the main view.  This includes, searching parts and products,
 * populating tableviews and handling the interaction with other components of the application.
 *
 * @author Devin Craig
 */
public class MainWindowController implements Initializable {

    Stage window;
    Parent scene;
    ObservableList<Part> partSearchList = FXCollections.observableArrayList();
    ObservableList<Product> productSearchList = FXCollections.observableArrayList();


    @FXML
    private Label partsTableErrorLabel;

    @FXML
    private Label productsTableErrorLabel;

    @FXML
    private Button exitButton;

    @FXML
    private Button addPartButton;

    @FXML
    private Button modifyPartButton;

    @FXML
    private Button deletePartButton;

    @FXML
    private TableView<Part> partsTable;

    @FXML
    private TableColumn<Part, Integer> partIdTableColumn;

    @FXML
    private TableColumn<Part, String> partNameTableColumn;

    @FXML
    private TableColumn<Part, Integer> partInventoryTableColumn;

    @FXML
    private TableColumn<Part, Double> partPriceTableColumn;

    @FXML
    private TextField partSearchField;

    @FXML
    private Button addProductButton;

    @FXML
    private Button modifyProductButton;

    @FXML
    private Button deleteProductButton;

    @FXML
    private TableView<Product> productsTable;

    @FXML
    private TableColumn<Product, Integer> productIdTableColumn;

    @FXML
    private TableColumn<Product, String> productNameTableColumn;

    @FXML
    private TableColumn<Product, Integer> productInventoryTableColumn;

    @FXML
    private TableColumn<Product, Double> productPriceTableColumn;

    @FXML
    private TextField productSearchField;

    /**
     * Add Part button handler. Loads the add part screen.
     * @param event addPart button clicked event.
     * @throws IOException Throws an IOException if the add part view can not be loaded.
     */
    @FXML
    void addPart(MouseEvent event) throws IOException {
        window = (Stage)((Button)event.getSource()).getScene().getWindow();
        scene = FXMLLoader.load(getClass().getResource("/view/add_part.fxml"));
        scene.getStylesheets().add(getClass().getResource("/styles/defaults.css").toExternalForm());
        window.setScene(new Scene(scene));
        window.show();
    }

    /**
     * Add Product button handler.  Loads the add product screen.
     * @param event addProduct button clicked event.
     * @throws IOException Throws an IOException if the add product view can not be loaded.
     */
    @FXML
    void addProduct(MouseEvent event) throws IOException {
        window = (Stage)((Button)event.getSource()).getScene().getWindow();
        scene = FXMLLoader.load(getClass().getResource("/view/add_product.fxml"));
        scene.getStylesheets().add(getClass().getResource("/styles/defaults.css").toExternalForm());
        window.setScene(new Scene(scene));
        window.show();
    }

    /**
     * Delete part button handler.  Requires user confirmation before removing the part from inventory.
     * This should be refactored in a future version to use Inventory class helpers.
     *
     * @param event deletePart button click event.
     */
    @FXML
    void deletePart(MouseEvent event) {
        partsTableErrorLabel.setText("");
        Part part = partsTable.getSelectionModel().getSelectedItem();
        if(part == null) {
            partsTableErrorLabel.setText("Please select a part.");
            return;
        }
        if(main.ConfirmationBox.display("Delete", "Are you sure you want to delete this part?")) {
                Inventory.getAllParts().remove(part);
        }
    }

    /**
     * Delete product button handler.  Requires user confirmation before removing the product from inventory.
     * This should be refactored in a future version to use Inventory class helpers.
     *
     * @param event deleteProduct button clicked event.
     */
    @FXML
    void deleteProduct(MouseEvent event) {
        productsTableErrorLabel.setText("");
        Product product = productsTable.getSelectionModel().getSelectedItem();
        if(product == null) {
            productsTableErrorLabel.setText("Please select a product.");
            return;
        }
        if(main.ConfirmationBox.display("Delete", "Are you sure you want to delete this product?")) {
            Inventory.getAllProducts().remove(product);
        }
    }

    /**
     * Exit Button handler.  Exits the program.
     * @param event exitProgramButton button clicked event.
     */
    @FXML
    void exitProgramButton(MouseEvent event) {
        window = (Stage)((Button)event.getSource()).getScene().getWindow();
        window.close();
    }

    /**
     * Modify part button handler.  Loads the modify part screen, passing the selected part to the modify part controller.
     * Sets the part error label if no part is selected.
     *
     * @param event modifyPart button clicked event.
     * @throws IOException Throws an IOException if the modifyPart view can not be loaded.
     */
    @FXML
    void modifyPart(MouseEvent event) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/modify_part.fxml"));
            loader.load();

            ModifyPartController MPController = loader.getController();
            MPController.sendPart(partsTable.getSelectionModel().getSelectedItem());
            window = (Stage)((Button)event.getSource()).getScene().getWindow();
            Parent scene = loader.getRoot();
            scene.getStylesheets().add(getClass().getResource("/styles/defaults.css").toExternalForm());
            window.setScene(new Scene(scene));
            window.show();
        } catch (NullPointerException e) {
            partsTableErrorLabel.setText("Please select a part.");
        }
    }

    /**
     * Modify product button handler.  Loads the modify product screen, passing the selected product to the modify product controller.
     * Sets the product error label if no product is selected.
     *
     * @param event modifyProduct button clicked event.
     * @throws IOException Throws an IOException if the modify product view can not be loaded.
     */
    @FXML
    void modifyProduct(MouseEvent event) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/modify_product.fxml"));
            loader.load();

            ModifyProductController MPController = loader.getController();
            MPController.sendProduct(productsTable.getSelectionModel().getSelectedItem());
            window = (Stage)((Button)event.getSource()).getScene().getWindow();
            Parent scene = loader.getRoot();
            scene.getStylesheets().add(getClass().getResource("/styles/defaults.css").toExternalForm());
            window.setScene(new Scene(scene));
            window.show();
        } catch (NullPointerException e) {
            productsTableErrorLabel.setText("Please select a product.");
        }
    }

    /**
     * Searches the parts in inventory by name or id.  Filters and updates the TableView on each KeyEvent.
     * @param event partSearch key event, triggers for each character in the search field.
     */
    @FXML
    void partSearch(KeyEvent event) {
        partSearchList.clear();
        for(Part part : Inventory.getAllParts()) {
            Integer id;
            try {
                id = Integer.parseInt(partSearchField.getText());
            } catch (NumberFormatException e) {
                id = -1;
            }
            if(part.getId() == id || part.getName().toLowerCase().contains(partSearchField.getText().toLowerCase())) {
                partSearchList.add(part);
            }
        }

        partsTable.setItems(partSearchList);
    }

    /**
     * Searches the products in inventory by name or id.  Filters and updates the TableView on each KeyEvent.
     * @param event productSearch key event, triggers for each character in the search field.
     */
    @FXML
    void productSearch(KeyEvent event) {
        productSearchList.clear();
        for(Product product : Inventory.getAllProducts()) {
            Integer id;
            try {
                id = Integer.parseInt(productSearchField.getText());
            } catch (NumberFormatException e) {
                id = -1;
            }
            if(product.getId() == id || product.getName().toLowerCase().contains(productSearchField.getText().toLowerCase())) {
                productSearchList.add(product);
            }
        }

        productsTable.setItems(productSearchList);
    }

    /**
     * Loads the inventory into the part and product lists, sets initial sorting, and formats the price columns.
     * @param url Implementation of Initializable
     * @param resourceBundle Implementation of Initializable
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        partsTable.setItems(Inventory.getAllParts());
        partIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        partNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        partInventoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("stock"));
        partPriceTableColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        partsTable.getSortOrder().add(partIdTableColumn);
        //Format Price https://stackoverflow.com/questions/48733121/javafx-format-double-in-tablecolumn
        NumberFormat partCurrencyFormat = NumberFormat.getCurrencyInstance();
        partPriceTableColumn.setCellFactory(tc -> new TableCell<Part, Double>(){
            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(partCurrencyFormat.format(price));
                }
            }
        });

        productsTable.setItems(Inventory.getAllProducts());
        productIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        productNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        productInventoryTableColumn.setCellValueFactory(new PropertyValueFactory<>("stock"));
        productPriceTableColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        productsTable.getSortOrder().add(productIdTableColumn);
        //Format Price https://stackoverflow.com/questions/48733121/javafx-format-double-in-tablecolumn
        NumberFormat productCurrencyFormat = NumberFormat.getCurrencyInstance();
        productPriceTableColumn.setCellFactory(tc -> new TableCell<Product, Double>(){
            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(productCurrencyFormat.format(price));
                }
            }
        });
    }
}