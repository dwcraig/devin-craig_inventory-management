package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.InHouse;
import model.Inventory;
import model.Outsourced;
import model.Part;

import java.io.IOException;

/**
 * The Modify Part Controller handles the functionality of the modify part view.  This class loads the currently saved part into the fields, and validates and saves updated parts into inventory.
 *
 * @author Devin Craig
 */
public class ModifyPartController {
    Stage window;
    Parent scene;
    Part tempPart;

    @FXML
    private Label partSourceLabel;

    @FXML // fx:id="outsourcedRadio"
    private RadioButton outsourcedRadio; // Value injected by FXMLLoader

    @FXML // fx:id="partSource"
    private ToggleGroup partSource; // Value injected by FXMLLoader

    @FXML // fx:id="inHouseRadio"
    private RadioButton inHouseRadio; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartName"
    private TextField modifyPartName; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartInventory"
    private TextField modifyPartInventory; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartPrice"
    private TextField modifyPartPrice; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartMinInventory"
    private TextField modifyPartMinInventory; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartMaxInventory"
    private TextField modifyPartMaxInventory; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartMachineID"
    private TextField modifyPartMachineID; // Value injected by FXMLLoader

    @FXML // fx:id="modifyPartID"
    private TextField modifyPartID; // Value injected by FXMLLoader

    @FXML // fx:id="saveButton"
    private Button saveButton; // Value injected by FXMLLoader

    @FXML // fx:id="cancelButton"
    private Button cancelButton; // Value injected by FXMLLoader

    @FXML
    private Label errorLabel;

    /**
     * Cancels adding the part.  User must confirm in the confirmation box in order to close the window.
     * @param event Cancel button clicked event.
     * @throws IOException Throws an IOException if the add main view can not be loaded.
     */
    @FXML
    void cancel(MouseEvent event) throws IOException {
        if(main.ConfirmationBox.display("Cancel", "Are you sure you want to cancel?")) {
            window = (Stage) ((Button) event.getSource()).getScene().getWindow();
            scene = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
            window.setScene(new Scene(scene));
            window.show();
        }
    }

    /**
     * inHouseSelected indicates when the in house radio button is selected.
     * Used to update the Machine Id / Company Name label and text field.
     * @param event In House radio button selected event.
     */
    @FXML
    void inHouseSelected(ActionEvent event) {
        partSourceLabel.setText("Machine ID");
    }

    /**
     * outsourcedSelected indicates when the outsourced radio button is selected.
     * Used to update the Machine Id / Company Name label and text field.
     * @param event Outsourced radio button selected event.
     */
    @FXML
    void outsourcedSelected(ActionEvent event) {
        partSourceLabel.setText("Company Name");
    }

    /**
     * Saves the part to inventory after validating the fields are within the correct bounds.
     * In a future version, this needs to be refactored to make use of helper functions in the Inventory class
     * In addition, this method handles changing between in house and outsourced parts.
     *
     * @param event Save Part button clicked event.
     * @throws IOException Throws an IOException if the add product view can not be loaded.
     */
    @FXML
    void savePart(MouseEvent event) throws IOException, Exception {
        try {
            checkValues();
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
            return;
        }
        if(tempPart instanceof InHouse && outsourcedRadio.isSelected()) {
//            Inventory.getAllParts().removeIf(part -> part.getId() == tempPart.getId());
            int index = 0;
            for(Part part : Inventory.getAllParts()) {
                if(tempPart.getId() == part.getId()) {
                    index = Inventory.getAllParts().indexOf(part);
                }
            }

            Inventory.getAllParts().set(index, new Outsourced(
                    tempPart.getId(),
                    modifyPartName.getText(),
                    Double.parseDouble(modifyPartPrice.getText()),
                    Integer.parseInt(modifyPartInventory.getText()),
                    Integer.parseInt(modifyPartMinInventory.getText()),
                    Integer.parseInt(modifyPartMaxInventory.getText()),
                    modifyPartMachineID.getText()
            ));
        } else if (tempPart instanceof Outsourced && inHouseRadio.isSelected()) {
            int index = 0;
            for(Part part : Inventory.getAllParts()) {
                if(tempPart.getId() == part.getId()) {
                    index = Inventory.getAllParts().indexOf(part);
                }
            }
//            Inventory.getAllParts().removeIf(part -> part.getId() == tempPart.getId());
            Inventory.getAllParts().set(index, new InHouse(
                    tempPart.getId(),
                    modifyPartName.getText(),
                    Double.parseDouble(modifyPartPrice.getText()),
                    Integer.parseInt(modifyPartInventory.getText()),
                    Integer.parseInt(modifyPartMinInventory.getText()),
                    Integer.parseInt(modifyPartMaxInventory.getText()),
                    Integer.parseInt(modifyPartMachineID.getText())
            ));
        } else {

            for(Part part : Inventory.getAllParts()) {
                if(part.getId() == tempPart.getId()) {
                    part.setName(modifyPartName.getText());
                    part.setStock(Integer.parseInt(modifyPartInventory.getText()));
                    part.setPrice(Double.parseDouble(modifyPartPrice.getText()));
                    part.setMin(Integer.parseInt(modifyPartMinInventory.getText()));
                    part.setMax(Integer.parseInt(modifyPartMaxInventory.getText()));
                    if(part instanceof InHouse) {
                        ((InHouse) part).setMachineId(Integer.parseInt(modifyPartMachineID.getText()));
                    } else {
                        ((Outsourced) part).setCompanyName(modifyPartMachineID.getText());
                    }
                }
            }
        }

        window = (Stage)((Button)event.getSource()).getScene().getWindow();
        scene = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
        window.setScene(new Scene(scene));
        window.show();
    }

    /**
     * Provides the ModifyPartController with access to the selected part to be modified from the Inventory.
     * @param part Receives a part from the caller
     */
    public void sendPart(Part part) {
        tempPart = part;
        modifyPartID.setText(String.valueOf(part.getId()));
        modifyPartName.setText(part.getName());
        modifyPartInventory.setText(String.valueOf(part.getStock()));
        modifyPartPrice.setText(String.valueOf(part.getPrice()));
        modifyPartMinInventory.setText(String.valueOf(part.getMin()));
        modifyPartMaxInventory.setText(String.valueOf(part.getMax()));

        if(part instanceof Outsourced) {
            modifyPartMachineID.setText(((Outsourced) part).getCompanyName());
            partSourceLabel.setText("Company Name");
            partSource.selectToggle(outsourcedRadio);
        }

        if(part instanceof InHouse) {
            modifyPartMachineID.setText(String.valueOf(((InHouse) part).getMachineId()));
            partSource.selectToggle(inHouseRadio);
        }
    }

    /**
     * Field validation handler to easily call more methods if changes were made.
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkValues() throws Exception {
        checkName();
        checkInventory();
        checkPrice();
        if(inHouseRadio.isSelected()) {
            checkMachineId();
        } else {
            checkCompanyName();
        }
    }

    /**
     * Checks to make sure that the company name is not an empty string.
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkCompanyName() throws Exception {
        if(modifyPartMachineID.getText().isEmpty()) {throw new Exception("Company Name is required.");}
    }

    /**
     * Checks to make sure that the part name is not an empty string.
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkName() throws Exception {
        if(modifyPartName.getText().isEmpty()) {throw new Exception("Name can't be empty.");}
    }

    /**
     * Completes the following checks on Inventory fields (stock, max, min):
     * Not empty
     * Min is less than Stock and Stock is less than Max
     * All fields can properly be parsed to Integers.
     *
     * @throws Exception to display in the error label to indicate what went wrong for the user.
     */
    private void checkInventory() throws Exception {
        if(modifyPartInventory.getText().isEmpty()) {throw new Exception("Inventory can't be empty.");}
        if(modifyPartMinInventory.getText().isEmpty()) {throw new Exception("Minimum Inventory can't be empty.");}
        if(modifyPartMaxInventory.getText().isEmpty()) {throw new Exception("Maximum Inventory can't be empty.");}

        try {
            int inv = Integer.parseInt(modifyPartInventory.getText());
            int minInv = Integer.parseInt(modifyPartMinInventory.getText());
            int maxInv = Integer.parseInt(modifyPartMaxInventory.getText());

            if(minInv > inv || maxInv < inv) {throw new Exception("Inventory must be between min and max");}
            if(minInv < 0 || inv < 0) {throw new Exception("Inventory must be a positive integer.");}

        } catch (NumberFormatException e){
            throw new Exception("All inventory values must be integers.");
        }
    }

    /**
     * Checks the following:
     * Not an empty field
     * Can properly be parsed as a double.
     *
     * @throws Exception to display in the error label to indicate what went wrong for the user.
     */
    private void checkPrice() throws Exception {
        Double price;
        if(modifyPartPrice.getText().isEmpty()) {throw new Exception("Price is required");}
        try {
            price = Double.parseDouble(modifyPartPrice.getText());
        } catch (NumberFormatException e) {
            throw new Exception("Price must be a Double");
        }

        if(price < 0) {throw new Exception("Price must be positive");}
    }

    /**
     * Checks that machine id is a positive Integer, and that it is not empty.
     * @throws Exception Throws an exception to be handled by the save part handler.
     */
    private void checkMachineId() throws Exception {
        int machineId;
        if(modifyPartMachineID.getText().isEmpty()) {throw new Exception("Machine Id is required.");}
        try {
            machineId = Integer.parseInt(modifyPartMachineID.getText());
        } catch (NumberFormatException e) {
            throw new Exception("Machine Id must be a Integer");
        }

        if(machineId < 0) {throw new Exception("Machine Id must be positive");}
    }
}

