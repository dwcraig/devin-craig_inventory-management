package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.InHouse;
import model.Inventory;
import model.Outsourced;

import java.io.IOException;
import java.util.Random;

/**
 * The Add Part Controller handles the functionality of the add part view.  This class validates and saves parts into inventory.
 *
 * @author Devin Craig
 */
public class AddPartController {
    Stage window;
    Parent scene;

    @FXML // fx:id = "partSourceLabel"
    private Label partSourceLabel;

    @FXML // fx:id="outsourcedRadio"
    private RadioButton outsourcedRadio; // Value injected by FXMLLoader

    @FXML // fx:id="partSource"
    private ToggleGroup partSource; // Value injected by FXMLLoader

    @FXML // fx:id="inHouseRadio"
    private RadioButton inHouseRadio; // Value injected by FXMLLoader

    @FXML // fx:id="addPartName"
    private TextField addPartName; // Value injected by FXMLLoader

    @FXML // fx:id="addPartInventory"
    private TextField addPartInventory; // Value injected by FXMLLoader

    @FXML // fx:id="addPartPrice"
    private TextField addPartPrice; // Value injected by FXMLLoader

    @FXML // fx:id="addPartMinInventory"
    private TextField addPartMinInventory; // Value injected by FXMLLoader

    @FXML // fx:id="addPartMaxInventory"
    private TextField addPartMaxInventory; // Value injected by FXMLLoader

    @FXML // fx:id="addPartMachineID"
    private TextField addPartMachineID; // Value injected by FXMLLoader

    @FXML // fx:id="addPartID"
    private TextField addPartID; // Value injected by FXMLLoader

    @FXML // fx:id="saveButton"
    private Button saveButton; // Value injected by FXMLLoader

    @FXML // fx:id="cancelButton"
    private Button cancelButton; // Value injected by FXMLLoader

    @FXML // fx:id="errorLabel"
    private Label errorLabel;

    /**
     * Cancels adding the part.  User must confirm in the confirmation box in order to close the window.
     * @param event
     * @throws IOException
     */
    @FXML
    void cancel(MouseEvent event) throws IOException {
        if(main.ConfirmationBox.display("Cancel", "Are you sure you want to cancel?")) {
            window = (Stage) ((Button) event.getSource()).getScene().getWindow();
            scene = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
            window.setScene(new Scene(scene));
            window.show();
        }
    }

    /**
     * inHouseSelected indicates when the in house radio button is selected.
     * Used to update the Machine Id / Company Name label and text field.
     * @param event In House radio button selected event.
     */
    @FXML
    void inHouseSelected(ActionEvent event) {
        partSourceLabel.setText("Machine ID");
    }

    /**
     * outsourcedSelected indicates when the outsourced radio button is selected.
     * Used to update the Machine Id / Company Name label and text field.
     * @param event Outsourced radio button selected event.
     */
    @FXML
    void outsourcedSelected(ActionEvent event) {
        partSourceLabel.setText("Company Name");
    }

    /**
     * Saves the part to inventory after validating the fields are within the correct bounds.
     * In a future version, this needs to be refactored to make use of helper functions in the Inventory class
     *
     * @param event Save part button clicked, begins validation of fields
     * @throws IOException Throws IOException if the main screen FXML can not be loaded.
     */
    @FXML
    void savePart(MouseEvent event) throws IOException {
        try {
            checkValues();
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
            return;
        }

        if(inHouseRadio.isSelected()){
            Inventory.getAllParts().add(new InHouse(
                    generateID(),
                    addPartName.getText(),
                    Double.parseDouble(addPartPrice.getText()),
                    Integer.parseInt(addPartInventory.getText()),
                    Integer.parseInt(addPartMinInventory.getText()),
                    Integer.parseInt(addPartMaxInventory.getText()),
                    Integer.parseInt(addPartMachineID.getText())));
        } else if (outsourcedRadio.isSelected()) {
            Inventory.getAllParts().add(new Outsourced(
                    generateID(),
                    addPartName.getText(),
                    Double.parseDouble(addPartPrice.getText()),
                    Integer.parseInt(addPartInventory.getText()),
                    Integer.parseInt(addPartMinInventory.getText()),
                    Integer.parseInt(addPartMaxInventory.getText()),
                    addPartMachineID.getText()
            ));
        }

        // go back to main screen
        window = (Stage)((Button)event.getSource()).getScene().getWindow();
        scene = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
        window.setScene(new Scene(scene));
        window.show();
    }

    /**
     * Generates a random ID between 1 and 500, and ensures it is unique.
     *
     * @return newId, provides a randomly generated, unique Id between 1 and 500.
     */
    private int generateID() {
        int newId = 0;
        Random generator = new Random();
        Boolean tryAgain = true;

        while(tryAgain) {
            newId = generator.nextInt(500);
            for (int i = 0; i < Inventory.getAllParts().size(); i++) {
                if (Inventory.getAllParts().get(i).getId() == newId || newId == 0) {
                    break;
                }
            }
            tryAgain = false;
        }

        return newId;
    }

    /**
     * Field validation handler to easily call more methods if changes were made.
     * @throws Exception Throws exception to be caught by the savePart handler.
     */
    private void checkValues() throws Exception {
        checkName();
        checkInventory();
        checkPrice();
        if(inHouseRadio.isSelected()) {
            checkMachineId();
        } else {
            checkCompanyName();
        }
    }

    /**
     * Checks to make sure that the company name is not an empty string.
     * @throws Exception Throws exception to be caught by the savePart handler.
     */
    private void checkCompanyName() throws Exception {
        if(addPartMachineID.getText().isEmpty()) {throw new Exception("Company Name is required.");}
    }

    /**
     * Checks to make sure that the part name is not an empty string.
     * @throws Exception Throws exception to be caught by the savePart handler.
     */
    private void checkName() throws Exception {
        if(addPartName.getText().isEmpty()) {throw new Exception("Name can't be empty.");}
    }

    /**
     * Completes the following checks on Inventory fields (stock, max, min):
     * Not empty
     * Min is less than Stock and Stock is less than Max
     * All fields can properly be parsed to Integers.
     *
     * @throws Exception to display in the error label to indicate what went wrong for the user.
     */
    private void checkInventory() throws Exception {
        if(addPartInventory.getText().isEmpty()) {throw new Exception("Inventory can't be empty.");}
        if(addPartMinInventory.getText().isEmpty()) {throw new Exception("Minimum Inventory can't be empty.");}
        if(addPartMaxInventory.getText().isEmpty()) {throw new Exception("Maximum Inventory can't be empty.");}

        try {
            int inv = Integer.parseInt(addPartInventory.getText());
            int minInv = Integer.parseInt(addPartMinInventory.getText());
            int maxInv = Integer.parseInt(addPartMaxInventory.getText());

            if(minInv > inv || maxInv < inv) {throw new Exception("Inventory must be between min and max");}
            if(minInv < 0 || inv < 0) {throw new Exception("Inventory must be a positive integer.");}

        } catch (NumberFormatException e){
            throw new Exception("All inventory values must be integers.");
        }
    }

    /**
     * Checks the following:
     * Not an empty field
     * Can properly be parsed as a double.
     *
     * @throws Exception to display in the error label to indicate what went wrong for the user.
     */
    private void checkPrice() throws Exception {
        Double price;
        if(addPartPrice.getText().isEmpty()) {throw new Exception("Price is required");}
        try {
            price = Double.parseDouble(addPartPrice.getText());
        } catch (NumberFormatException e) {
            throw new Exception("Price must be a Double");
        }

        if(price < 0) {throw new Exception("Price must be positive");}
    }

    /**
     * Checks that machine id is a positive Integer, and that it is not empty.
     * @throws Exception
     */
    private void checkMachineId() throws Exception {
        int machineId;
        if(addPartMachineID.getText().isEmpty()) {throw new Exception("Machine Id is required.");}
        try {
            machineId = Integer.parseInt(addPartMachineID.getText());
        } catch (NumberFormatException e) {
            throw new Exception("Machine Id must be a Integer");
        }

        if(machineId < 0) {throw new Exception("Machine Id must be positive");}
    }

}

