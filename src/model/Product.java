package model;

import javafx.collections.ObservableList;

/**
 *
 * Product provides the model for how products will be stored and managed.
 *
 * @author Devin Craig
 */
public class Product {
    private ObservableList<Part> associatedParts;
    private int id;
    private String name;
    private double price;
    private int stock;
    private int min;

    /**
     *
     * @return associatedParts
     */
    public ObservableList<Part> getAssociatedParts() {
        return associatedParts;
    }

    /**
     * Sets associatedParts for the product.
     * @param associatedParts Parts associated with the product.
     */
    public void setAssociatedParts(ObservableList<Part> associatedParts) {
        this.associatedParts = associatedParts;
    }

    /**
     *
     * @return Product id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name of the product.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * The name to set.
     *
     * @param name Name to set product name to.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the price of the product.
     *
     * @return price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets the product price.
     *
     * @param price Price to set part to.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     *
     * @return the stock
     */
    public int getStock() {
        return stock;
    }

    /**
     * Sets the current inventory.
     *
     * @param stock Set the amount of stock in inventory.
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     *
     * @return the minimum inventory
     */
    public int getMin() {
        return min;
    }

    /**
     * Sets the minimum inventory
     * @param min Minimum inventory level to set.
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     *
     * @return the maximum inventory
     */
    public int getMax() {
        return max;
    }

    /**
     * Sets the maximum inventory.
     * @param max Maximum inventory level to set.
     */
    public void setMax(int max) {
        this.max = max;
    }

    private int max;

    /**
     * Construct a new instance of a product.
     *
     * @param associatedParts Parts associated with the product.
     * @param id Id of the product.
     * @param name Name of the product.
     * @param price Price of the product.
     * @param stock Amount of stock in inventory.
     * @param min Minimum inventory level.
     * @param max Maximum inventory level.
     */
    public Product(ObservableList<Part> associatedParts, int id, String name, double price, int stock, int min, int max) {
        this.associatedParts = associatedParts;
        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.min = min;
        this.max = max;
    }
}
