package model;

/**
 * The Outsourced class provides the model for parts that are Outsourced.
 *
 * @author Devin Craig
 */
public class Outsourced extends Part {

    private String companyName;

    /**
     * Construct a new instance of an Outsourced part.
     *
     * @param id Part id
     * @param name Name of the part.
     * @param price Price of the part.
     * @param stock Amount of stock in inventory.
     * @param min Minimum inventory level.
     * @param max Maximum inventory level.
     * @param companyName Name of the company providing the product.
     */
    public Outsourced(int id, String name, double price, int stock, int min, int max, String companyName) {
        super(id, name, price, stock, min, max);
        this.companyName = companyName;
    }

    /**
     *
     * @return the name of the company that provides the part
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the company name that provides the part.
     *
     * @param companyName Name of the company providing the product.
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
