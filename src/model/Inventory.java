package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.stream.Collectors;

/**
 * The Inventory class is where our parts and products are stored and tracked.  It provides helper functions when managing
 * the pieces of inventory, as well as the data stuctures to hold inventory.
 *
 * @author Devin Craig
 */
public class Inventory {


    private static ObservableList<Part> allParts = FXCollections.observableArrayList();
    private static ObservableList<Product> allProducts = FXCollections.observableArrayList();;

    /**
     * Add a part to the inventory.
     * @param newPart New part to be added to inventory.
     */
    public static void addPart(Part newPart) {
        allParts.add(newPart);
    }

    /**
     * Add a new product to the inventory
     * @param newProduct New product to be added to inventory.
     */
    public static void addProduct(Product newProduct) {
        allProducts.add(newProduct);
    }


    /**
     * Unused currently, implemented search directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param partId Id of the part being searched for.
     * @return the product with the partId passed in.
     */
    public static Part lookupPart(int partId) {
        return allParts.stream()
                .filter(part -> part.getId() == partId)
                .findFirst().get();
    }

    /**
     * Unused currently, implemented search directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param partName The part name being searched for.
     * @return The part with the name passed in.
     */
    // Stack Overflow answer here for filter/collection solution: https://stackoverflow.com/questions/33849538/collectors-lambda-return-observable-list
    public static ObservableList<Part> lookupPart(String partName) {
        return allParts.stream()
                .filter(part ->  part.getName().contains(partName))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));
    }

    /**
     * Unused currently, implemented search directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param productId Id of the product being searched for.
     * @return The product with the Id passed in
     */
    public static Product lookupProduct(int productId) {
        return allProducts.stream()
                .filter(prod -> prod.getId() == productId)
                .findFirst().get();
    }

    /**
     * Unused currently, implemented search directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param productName The name of the product being searched for.
     * @return The product with the name passed in.
     */
    public static ObservableList<Product> lookupProduct(String productName) {
        return allProducts.stream()
                .filter(product ->  product.getName().contains(productName))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));
    }

    /**
     * Unused currently, implemented directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param index Index of the part to be updated.
     * @param selectedPart Part to set at index.
     */
    public static void updatePart(int index, Part selectedPart) {
        allParts.set(index, selectedPart);
    }

    /**
     * Unused currently, implemented directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param index Index of product to be updated.
     * @param newProduct Product to be set at index.
     */
    public static void updateProduct(int index, Product newProduct) {
        allProducts.set(index, newProduct);
    }

    /**
     * Unused currently, implemented directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param selectedPart Part to be deleted.
     * @return A boolean to check status of a part deletion.
     */
    public static boolean deletePart(Part selectedPart) {
        return allParts.remove(selectedPart);
    }

    /**
     * Unused currently, implemented directly in the controller.
     * In a future version, search functionality could be moved here for
     * better reuse of code, and to increase maintainability.
     *
     * @param selectedProduct Product to be deleted.
     * @return A boolean to check status of a product deletion.
     */
    public static boolean deleteProduct(Product selectedProduct) {
        return allProducts.remove(selectedProduct);
    }

    /**
     *
     * @return the list of all parts
     */
    public static ObservableList<Part> getAllParts() {
        return allParts;
    }

    /**
     *
     * @return the list of all products
     */
    public static ObservableList<Product> getAllProducts() {
        return allProducts;
    }
}
