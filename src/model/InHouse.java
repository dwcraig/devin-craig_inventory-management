package model;

/**
 * Provides the model for the In House part.
 *
 * @author Devin Craig
 */
public class InHouse extends Part {

    private int machineId;

    /**
     * Construct a new In House Part instance.
     *
     * @param id Unique part Id
     * @param name The name of the part
     * @param price Price of the part
     * @param stock The amount of stock in Inventory.
     * @param min Minimum inventory level.
     * @param max Maximum inventory level.
     * @param machineId The Id of the machine used to product the part.
     */
    public InHouse(int id, String name, double price, int stock, int min, int max, int machineId) {
        super(id, name, price, stock, min, max);
        this.machineId = machineId;
    }

    /**
     *
     * @return the machine id
     */
    public int getMachineId() {
        return machineId;
    }

    /**
     * Set the machine Id.
     *
     * @param machineId Called when creating the part, the machine Id of the machine used to make the part.
     */
    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }
}
